#pragma once
class vehicle
{
public:
	float maxWeight;
	float maxVolume;

	vehicle(float weight, float volume) {
		maxVolume = volume;
		maxWeight = weight;
	}

	vehicle(int id, float weight, float volume) {
		maxVolume = volume;
		maxWeight = weight;
	}

	bool operator > (vehicle &item)
	{
		bool res = item.maxWeight > maxWeight;
		if (item.maxWeight == maxWeight) res = item.maxVolume > maxVolume;
		return res;
	}
	bool operator < (const vehicle &item) {
		bool res = item.maxWeight < maxWeight;
		if (item.maxWeight == maxWeight) res = item.maxVolume < maxVolume;
		return res;
	}
	void print();
};

